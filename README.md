Run this command to install dependencies for the frontend and backend:

```
./setup.sh
```

Then, start docker:

```
docker-compose up
```

Then, start the backend:
```
./start-backend.sh
```

Then, start the frontend:
```
./start-frontend.sh
```

The specific code created or modified by me for the frontend are:

```
frontend/src/App.css
frontend/src/App.js
frontend/src/index.js
frontend/src/store.js
frontend/src/component/Cart.js
frontend/src/component/Checkout.js
frontend/src/component/Navigation.js
frontend/src/component/Order.js
frontend/src/component/OrderList.js
frontend/src/component/Product.js
frontend/src/component/ProductList.js
```

The specific code created or modified by me for the backend are:

```
backend/src/app.js
backend/src/index.js
backend/src/routes.js
backend/src/lib/cassandra.js
backend/src/lib/checkout.js
backend/src/lib/model
backend/src/lib/model/cart-item.js
backend/src/lib/model/order.js
backend/src/lib/model/product-item.js
backend/src/lib/model/product.js
backend/src/lib/model/session.js
backend/src/lib/mysql.js
backend/src/lib/session-manager.js
```

