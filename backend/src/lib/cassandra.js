import cassandra from 'cassandra-driver';

let instance = null;

export default () => {
    if (!instance) {
        // @TODO define environment vars for contactPoints and keyspace
        instance = new cassandra.Client({ contactPoints: ['localhost'], keyspace: 'app' });
    }
    return instance;
};
