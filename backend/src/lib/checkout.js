import Order from './model/order';

export default class {
    placeOrder({ userId, cartItems }) {
        const orderModel = new Order();
        return orderModel.createOrder({
            userId,
            cartItems
        });
    }
}
