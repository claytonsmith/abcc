export default class {
    constructor({ id, quantity, price, name }) {
        this.id = id;
        this.quantity = quantity;
        this.price = price;
        this.name = name;
    }
}
