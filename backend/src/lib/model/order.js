import _ from 'underscore';
import Promise from 'bluebird';
import mysql from '../mysql';

export default class {
    fetchAll({ userId }) {
        const sql = 'SELECT * FROM `order` WHERE user_id=? ORDER BY created_timestamp DESC';
        return mysql()
            .queryAsync(sql, [userId])
            .catch(e => {
                throw e;
            });
    }
    fetch({ orderId }) {
        const sql = 'SELECT * FROM `order` WHERE id=?';
        return mysql()
            .queryAsync(sql, [orderId])
            .then(rows => rows[0])
            .catch(e => {
                throw e;
            });
    }
    fetchLines({ orderId }) {
        const sql = 'SELECT * FROM `order_lines` WHERE order_id=?';
        return mysql()
            .queryAsync(sql, [orderId])
            .catch(e => {
                throw e;
            });
    }
    createOrder({ userId, cartItems }) {
        return mysql()
            .beginTransactionAsync()
            .catch(this._rollback)
            .then(() => {
                return this._createOrder({ userId, cartItems })
                    .catch(this._rollback)
                    .then(result => ({
                        orderId: result.insertId
                    }));
            })
            .then(({ orderId }) => {
                return this._createLines({ orderId, cartItems }).catch(this._rollback);
            })
            .then(() => mysql().commitAsync());
    }
    _createOrder({ userId, cartItems }) {
        const total = _.reduce(
            cartItems,
            (memo, { quantity, price }) => {
                return memo + quantity * price;
            },
            0
        );
        const sql = `INSERT INTO \`order\` (user_id, total) VALUES(?, ?)`;
        const values = [userId, total];
        return mysql().queryAsync(sql, values);
    }
    _createLines({ orderId, cartItems }) {
        return Promise.map(cartItems, ({ id, name, quantity, price }) => {
            const sql = `
                INSERT INTO order_lines
                (order_id, item_id, name, quantity, price)
                VALUES(?, ?, ?, ?, ?)`;
            const values = [orderId, id, name, quantity, price];
            return mysql()
                .queryAsync(sql, values)
                .catch(this._rollback);
        });
    }
    _rollback(error) {
        console.log(error);
        return mysql()
            .rollbackAsync()
            .then(() => Promise.reject('Order creation failed'));
    }
}
