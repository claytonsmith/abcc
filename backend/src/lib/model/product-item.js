import mysql from '../mysql';

export default class {
    fetch(id) {
        const sql = `
            SELECT p.id, CONCAT(p.name, ': ', pi.name) AS name, price 
            FROM product_items pi
            INNER JOIN product p ON p.id = pi.product_id
            WHERE pi.id=?`;
        const values = [id];
        return mysql()
            .queryAsync(sql, values)
            .then(result => {
                if (result.length === 0) throw new Error('Invalid item');
                this.id = id;
                this.name = result[0].name;
                this.price = result[0].price;
                return this;
            })
            .catch(e => {
                throw e;
            });
    }
}
