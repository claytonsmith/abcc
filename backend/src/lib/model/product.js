import _ from 'underscore';
import mysql from '../mysql';

export default class {
    fetchAll() {
        const sql = `
            SELECT 
              p.id
            , p.name
            , pi.id AS item_id
            , pi.name AS item_name
            , p.price
            FROM product p
            INNER JOIN product_items pi ON p.id=pi.product_id`;
        return mysql()
            .queryAsync(sql)
            .then(result => {
                return _.reduce(
                    result,
                    (memo, product) => {
                        if (_.findWhere(memo, { id: product.id })) {
                            memo = memo.map(p => ({
                                ...p,
                                items:
                                    p.id === product.id
                                        ? [
                                              ...p.items,
                                              {
                                                  id: product.item_id,
                                                  name: product.item_name,
                                                  price: product.price
                                              }
                                          ]
                                        : p.items
                            }));
                        } else {
                            memo.push({
                                id: product.id,
                                name: product.name,
                                price: product.price,
                                items: [
                                    {
                                        id: product.item_id,
                                        name: product.item_name,
                                        price: product.price
                                    }
                                ]
                            });
                        }
                        return memo;
                    },
                    []
                );
            })
            .catch(e => {
                throw e;
            });
    }
}
