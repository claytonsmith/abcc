import cassandra from '../cassandra';
export default class {
    createSession({ userId, session }) {
        return cassandra().execute(
            'INSERT INTO "app"."session" (id, session) VALUES(?, ?)',
            [userId, JSON.stringify(session)],
            { prepare: true }
        );
    }
    getSession({ userId }) {
        return cassandra().execute('SELECT id, session FROM "app"."session" WHERE id=?', [userId], { prepare: true });
    }
    update({ userId, session }) {
        return cassandra().execute(
            'UPDATE "app"."session" SET session=? WHERE id=?',
            [JSON.stringify(session), userId],
            { prepare: true }
        );
    }
}
