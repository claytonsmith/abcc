import mysql from 'mysql';
import mysqlConnection from 'mysql/lib/Connection';
import Promise from 'bluebird';

Promise.promisifyAll(mysqlConnection.prototype);

let instance = null;

export default () => {
    if (!instance) {
        // @TODO use environment vars for connection string
        instance = mysql.createConnection('mysql://root:test@localhost:3310/app');
    }
    return instance;
};
