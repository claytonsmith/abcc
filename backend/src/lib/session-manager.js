import _ from 'underscore';
import CartItem from './model/cart-item';
import ProductItem from './model/product-item';
import Session from './model/session';

const defaultSession = {
    cartItems: []
};

export default class SessionManager {
    constructor(userId) {
        this.userId = userId;
    }
    loadSession() {
        const sessionModel = new Session();
        return sessionModel.getSession({ userId: this.userId }).then(result => {
            if (result.rows.length === 1) {
                this.session = JSON.parse(result.rows[0].session);
                return this.session;
            }
            return sessionModel.createSession({ userId: this.userId, session: defaultSession }).then(() => {
                this.session = defaultSession;
                return defaultSession;
            });
        });
    }
    async addItem(productItemId, quantity) {
        if (quantity < 1) {
            throw new Error('Invalid quantity');
        }

        const newItem = new ProductItem();
        await newItem.fetch(productItemId);

        const newCartItem = new CartItem({
            id: productItemId,
            quantity: quantity,
            price: newItem.price,
            name: newItem.name
        });

        if (_.findWhere(this.session.cartItems, { id: newCartItem.id })) {
            this.session.cartItems = this.session.cartItems.map(item => {
                if (newCartItem.id === item.id) {
                    return {
                        ...item,
                        quantity: +item.quantity + +newCartItem.quantity
                    };
                }
                return item;
            });
        } else {
            this.session.cartItems.push(newCartItem);
        }
        return this;
    }
    emptyCart() {
        this.session.cartItems = [];
        return this;
    }
    persist() {
        const sessionModel = new Session();
        return sessionModel
            .update({ userId: this.userId, session: this.session })
            .then(result => {
                return true;
            })
            .catch(() => {
                throw new Error('Error persisting session');
            });
    }
}
