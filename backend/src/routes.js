import { Router } from 'express';
import SessionManager from './lib/session-manager';
import Checkout from './lib/checkout';
import Product from './lib/model/product';
import Order from './lib/model/order';

const routes = Router();

// View all items in session, including the cart items
routes.get('/session/:userId', (req, res) => {
    const sessionManager = new SessionManager(req.params.userId);
    sessionManager.loadSession().then(session => {
        res.json(session);
    });
});

// Add item to the cart
routes.put('/cart', (req, res) => {
    const sessionManager = new SessionManager(req.body.userId);
    sessionManager.loadSession().then(session => {
        const { id, quantity } = req.body;

        sessionManager
            .addItem(id, quantity)
            .then(() => sessionManager.persist())
            .then(() => {
                res.json(sessionManager.session);
            });
    });
});

// Get products
routes.get('/products', (req, res) => {
    const product = new Product();
    product.fetchAll().then(result => res.json(result));
});

// Get orders
routes.get('/orders/:userId', (req, res) => {
    const order = new Order();
    order.fetchAll({ userId: req.params.userId }).then(result => res.json(result));
});

// Get orders
routes.get('/order/:orderId', (req, res) => {
    // @NOTE ignores user id for PoC
    const orderModel = new Order();
    orderModel
        .fetch({ orderId: req.params.orderId })
        .then(order => {
            return orderModel.fetchLines({ orderId: req.params.orderId }).then(orderLines => {
                return {
                    order,
                    orderLines
                };
            });
        })
        .then(result => res.json(result));
});

// Place an order
routes.post('/order', (req, res) => {
    const sessionManager = new SessionManager(req.body.userId);
    sessionManager.loadSession().then(session => {
        const checkout = new Checkout();
        checkout
            .placeOrder({
                userId: sessionManager.userId,
                cartItems: session.cartItems
            })
            .then(() => {
                sessionManager.emptyCart();
                sessionManager.persist();
                res.sendStatus(200);
            })
            .catch(() => res.sendStatus(500));
    });
});

export default routes;
