import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import Navigation from './component/Navigation';
import ProductList from './component/ProductList';
import Checkout from './component/Checkout';
import OrderList from './component/OrderList';
import Order from './component/Order';
import './App.css';
import store from './store';

class App extends Component {
    render() {
        return (
            <Provider store={store()}>
                <div className="App">
                    <Router>
                        <div>
                            <Navigation />
                            <Route exact path="/" component={ProductList} />
                            <Route path="/checkout" component={Checkout} />
                            <Route path="/purchase-history" component={OrderList} />
                            <Route path="/order/:orderId" component={Order} />
                        </div>
                    </Router>
                </div>
            </Provider>
        );
    }
}

export default App;
