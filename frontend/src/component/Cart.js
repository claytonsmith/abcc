import * as React from 'react';
import { connect } from 'react-redux';
import store from '../store';
import { Glyphicon } from 'react-bootstrap';

export const updateCartAction = cartItems => {
    store().dispatch({
        type: 'UPDATE_CART',
        cartItems
    });
    localStorage.setItem('session', JSON.stringify({ cartItems }));
};

const Cart = class extends React.Component {
    componentDidMount() {
        const sessionStorage = localStorage.getItem('session');
        if (sessionStorage) {
            try {
                updateCartAction(JSON.parse(sessionStorage).cartItems);
            } catch (e) {
                //throw new Error('Error loading session');
            }
        }
        
        // always load from session for source of truth
        fetch('http://localhost:8080/session/1')
            .then(response => response.json())
            .then(session => {
            updateCartAction(session.cartItems);
        });

    }
    render() {
        const { cartItems } = this.props;
        const total = cartItems.reduce((memo, { quantity }) => memo + parseInt(quantity, 10), 0);
        return (
            <div>
                <Glyphicon glyph="shopping-cart" /> {total} items
            </div>
        );
    }
};

const mapDispatchToProps = ({ cartItems }) => {
    return {
        cartItems
    };
};

export default connect(mapDispatchToProps)(Cart);
