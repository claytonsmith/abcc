import * as React from 'react';
import { connect } from 'react-redux';
import { Table, Button, Alert } from 'react-bootstrap';
import { updateCartAction } from './Cart';

export const OrderTable = ({ items }) => {
    const total = items
        .reduce((memo, { quantity, price }) => memo + parseFloat(quantity, 10) * parseFloat(price, 10), 0)
        .toFixed(2);
    return (
        <Table striped bordered condensed hover>
            <thead>
                <tr>
                    <th>Product</th>
                    <th>Quantiy</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {items.map(item => (
                    <tr key={item.id}>
                        <td>
                            <h3>{item.name}</h3>
                        </td>
                        <td>
                            <h3>{item.quantity}</h3>
                        </td>
                        <td>
                            <h3>${item.price}</h3>
                        </td>
                    </tr>
                ))}
                <tr>
                    <td colSpan="2">
                        <h4>Total</h4>
                    </td>
                    <td>
                        <h4>${total}</h4>
                    </td>
                </tr>
            </tbody>
        </Table>
    );
};

const Checkout = class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            status: 'pending'
        };
        this.handleOrderSubmit = this.handleOrderSubmit.bind(this);
    }
    handleOrderSubmit() {
        const body = {
            userId: 1
        };
        fetch('http://localhost:8080/order', {
            method: 'post',
            body: JSON.stringify(body),
            headers: { 'content-type': 'application/json' }
        })
            .then(() => {
                updateCartAction([]);
                this.setState({ status: 'order_completed' });
            })
            .catch(() => {
                this.setState({ status: 'error' });
            });
    }
    render() {
        const { cartItems } = this.props;
        const { status } = this.state;

        if (status === 'pending' && cartItems.length === 0) {
            return <div>Your cart is empty</div>;
        }

        if (status === 'order_completed') {
            return <div>Thank you for your order!</div>;
        }

        return (
            <div style={{ margin: '0 auto', width: 500, padding: 10 }}>
                <OrderTable items={cartItems} />
                <div style={{ padding: 10 }}>
                    <Button onClick={this.handleOrderSubmit} style={{ padding: 10 }}>
                        Submit Order
                    </Button>
                </div>
                {status === 'error' && <Alert bsStyle="danger">Error submitting the order! Please try again.</Alert>}
            </div>
        );
    }
};

const mapStateToProps = ({ cartItems }) => {
    return {
        cartItems
    };
};

export default connect(mapStateToProps)(Checkout);
