import * as React from 'react';
import { Nav, NavItem } from 'react-bootstrap';
import { withRouter } from 'react-router-dom';
import Cart from './Cart';

const Navigation = ({ history }) => (
    <Nav bsStyle="tabs" onSelect={key => history.push(key)}>
        <NavItem eventKey="/">
            <h3>Products</h3>
        </NavItem>
        <NavItem eventKey="/purchase-history">
            <h3>Purchase History</h3>
        </NavItem>
        <NavItem eventKey="/checkout">
            <h3>
                <Cart />
            </h3>
        </NavItem>
    </Nav>
);

export default withRouter(Navigation);
