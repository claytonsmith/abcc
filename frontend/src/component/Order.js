import * as React from 'react';
import moment from 'moment';
import { withRouter } from 'react-router';
import { OrderTable } from './Checkout';

const Order = class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            order: undefined,
            orderLines: undefined,
            isLoading: true
        };
    }
    componentDidMount() {
        fetch(`http://localhost:8080/order/${this.props.match.params.orderId}`)
            .then(response => response.json())
            .then(({ order, orderLines }) => {
                this.setState({
                    order,
                    orderLines,
                    isLoading: false
                });
            });
    }
    render() {
        const { isLoading, order, orderLines } = this.state;
        if (isLoading) return <div>Loading</div>;
        return (
            <div style={{ margin: '0 auto', width: 500, padding: 10 }}>
                <h2>Order #{order.id}</h2>
                <h4>{moment(order.created_timestamp).format('MMM D, YYYY hh:mma')}</h4>
                <OrderTable items={orderLines} />
            </div>
        );
    }
};

export default withRouter(Order);
