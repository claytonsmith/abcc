import * as React from 'react';
import { Table } from 'react-bootstrap';
import moment from 'moment';
import { Link } from 'react-router-dom';

export default class OrderList extends React.Component {
    constructor() {
        super();
        this.state = {
            isLoading: true,
            orders: []
        };
    }
    componentDidMount() {
        fetch('http://localhost:8080/orders/1')
            .then(response => response.json())
            .then(orders => {
                this.setState({
                    orders,
                    isLoading: false
                });
            });
    }
    render() {
        if (this.state.isLoading) {
            return <div>Loading</div>;
        }

        if (this.state.orders.length === 0) {
            return <div>You haven't placed any orders yet.</div>;
        }

        return (
            <div style={{ margin: '0 auto', width: 500 }}>
                <h3>Your Purchase History</h3>
                <Table striped bordered condensed hover>
                    <tbody>
                        {this.state.orders.map(order => (
                            <tr key={order.id}>
                                <td>Order #{order.id}</td>
                                <td>{moment(order.created_timestamp).format('MMM D, YYYY hh:mma')}</td>
                                <td style={{ textAlign: 'left' }}>${order.total}</td>
                                <td>
                                    <Link to={`/order/${order.id}`}>Details</Link>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </Table>
            </div>
        );
    }
}
