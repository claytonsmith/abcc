import * as React from 'react';
import { Button, Form, FormControl } from 'react-bootstrap';
import { updateCartAction } from './Cart';

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: props.items[0].id,
            quantity: 1
        };
        this.handleAddToCart = this.handleAddToCart.bind(this);
    }
    handleAddToCart() {
        const body = {
            userId: 1,
            id: this.state.value,
            quantity: this.state.quantity
        };
        fetch('http://localhost:8080/cart', {
            method: 'put',
            body: JSON.stringify(body),
            headers: { 'content-type': 'application/json' }
        })
            .then(response => response.json())
            .then(data => {
                updateCartAction(data.cartItems);
            });
    }
    render() {
        const { name, price, items } = this.props;
        return (
            <div>
                <h2>{name}</h2>
                <h4>${price}</h4>
                <Form inline>
                    <FormControl
                        componentClass="select"
                        value={this.state.value}
                        onChange={event => this.setState({ value: event.target.value })}
                        style={{ width: 120 }}
                    >
                        {items.map(item => (
                            <option key={item.id} value={item.id}>
                                {item.name}
                            </option>
                        ))}
                    </FormControl>{' '}
                    <FormControl
                        type="number"
                        value={this.state.quantity}
                        onChange={event => this.setState({ quantity: event.target.value })}
                        min="1"
                        style={{ width: 50 }}
                    />{' '}
                    <Button onClick={this.handleAddToCart}>Add to Cart</Button>
                </Form>
            </div>
        );
    }
}
