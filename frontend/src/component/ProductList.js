import * as React from 'react';
import Product from './Product';

export default class ProductList extends React.Component {
    constructor() {
        super();
        this.state = {
            isLoading: true,
            products: []
        };
    }
    componentDidMount() {
        fetch('http://localhost:8080/products')
            .then(response => response.json())
            .then(products => {
                this.setState({
                    products,
                    isLoading: false
                });
            });
    }
    render() {
        if (this.state.isLoading) {
            return <div>Loading</div>;
        }

        return <div>{this.state.products.map(product => <Product key={product.id} {...product} />)}</div>;
    }
}
