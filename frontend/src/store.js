import { createStore } from 'redux';

let instance = null;

function appReducer(state = [], action) {
    switch (action.type) {
        case 'UPDATE_CART':
            return {
                ...state,
                cartItems: action.cartItems
            };
        default:
            return state;
    }
}

const initialState = {
    cartItems: []
};

export const initialize = () => {
    if (!instance) {
        instance = createStore(appReducer, initialState);
    }
};

export default () => instance;
